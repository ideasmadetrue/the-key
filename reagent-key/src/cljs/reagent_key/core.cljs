
(ns reagent-key.core
    (:require-macros [secretary.core :refer [defroute]])
    (:import goog.History)
    (:require [secretary.core :as secretary]
              [goog.events :as events]
              [goog.history.EventType :as EventType]
              [reagent.core :as reagent :refer [atom]]))

;[reagent.session :as session]
;; -------------------------
;; Environment

; (defonce window-width (reagent/atom (* 1.0 js/window.innerWidth)))
; (defonce window-height (reagent/atom (* 1.0 js/window.innerHeight)))

(def app-state (reagent/atom {:width (.-innerWidth js/window)
               :height (.-innerHeight js/window)
               
               }))
(def page-state (reagent/atom {}))
(def german (atom false))
(defn german-english [] 
  (if @german  (reset! german false) (reset! german true)) 
  )
(defonce pages-total (atom  9))

(defn hook-browser-navigation! []
  (doto (History.)
    (events/listen
     EventType/NAVIGATE
     (fn [event]
       (secretary/dispatch! (.-token event))))
    (.setEnabled true)))




(def sliderpages {
               

               ; {  :key "slide-0"
               ;  :title-de "page one" :text-de "Lorem Ipsum Lorem " :class "pii"
               ;  :image "Nigel-Lamb-and-son.png" :width 1920 :height 1280

                    ; }
                :slide1
                 {  :key "slide-1"
                  
                  :title-en (seq ["360"  "brand" "experience"]) :text-en "THE KEY provides 360° brand staging with strong emotional appeal."
                  :title-de (seq ["360"  "brand" "experience"]) :text-de "THE KEY steht für eine 360° Markeninszenierung voller Emotionen." :class "animated.infinite"
                :image "360.jpg" :width 1280:height 1920

                }


                :slide2
                 {  :key "slide-2"
                 :title-en "NO LIMITS" :text-en "is our motto."
                  :title-de "NO LIMITS" :text-de "lautet unser Motto." :class "animated.infinite"
                :image "keine_kompromisse.jpg" :width 1280:height 1920

                }

                 :slide3
                 {  :key "slide-3"
                 :title-en (seq ["BEHIND" "THE SCENES"]) :text-en "Trust-based collaboration with our clients guarantees outstanding results."
                  :title-de (seq ["BEHIND" "THE SCENES"]) :text-de (seq ["Die vertrauensvolle Zusammenarbeit mit unseren Kunden garantiert besondere Ergebnisse."]) :class "pii"
                :image "behind_the_scenes.jpg" :width 1280:height 1919

                }

                 :slide4
                 {  :key "slide-4"
                 :title-en "JOHN TRAVOLTA" :text-en "saw it. He wanted it. He got it. Our Breitling Jet Team flightsuit at Le Bourget. Enjoy it, John!"
                  :title-de "JOHN TRAVOLTA" :text-de (seq ["Er sah ihn. Er wollte ihn. Er bekam ihn. Unseren Breitling Jet Team Flightsuit, in Le Bourget. Viel Spaß, John!"]) :class "pii"
                :image "travolta.jpg" :width 1945 :height 1280

                }

                 :slide5
                 {  :key "slide-5"
                  :title-en "AIRCRAFT DESIGN" :text-en "For our clients, we design everything involved in the presentation of the brand. Even aeroplanes!"
                  :title-de "FLUGZEUG DESIGN" :text-de (seq ["Für unsere Kunden gestalten wir alles, was zur Inszenierung der Marke beiträgt. Gerne auch Flugzeuge!"]) :class "rechts"
                :image "flugzeug_design.jpg" :width 1945 :height 1280

                }

                :slide6
                {   :key "slide-6"
                  :title-en "CLUB DESIGN" :text-en "We design unique meeting places for exclusive requirements of the international clientele of major brands."
                  :title-de "CLUB DESIGN" :text-de (seq ["Wir entwerfen einzigartige Begegnungsorte für exklusive Ansprüche des internationalen Publikums großer Marken."]) :class "pii"
                :image "club_design.jpg" :width 1945 :height 1280
                }


                :slide7
                {
                    :key "slide-7"
                  :title-en "SHOW DESIGN" :text-en "There is an inspiring space between dream and reality. That is where we take our audience."
                  :title-de "SHOW DESIGN" :text-de (seq ["Es gibt diesen inspirierenden Raum zwischen Traum und Wirklichkeit. Dorthin entführen wir unser Publikum."]) :class "pii"
                :image "show_design.jpg" :width 1945 :height 1280
                }


                :slide8
                { 
                    :key "slide-8"
                    :title-en "A NEIGHBOUR" :text-en "was so fascinated by our staging at Baselworld that he sent us this photo. Thank you, neighbour!"
                  :title-de "EIN NACHBAR" :text-de (seq ["Ein Nachbar war von einer unserer Inszenierungen auf der Basel World so fasziniert, dass er uns dieses Foto schickte. Danke, lieber Nachbar!"]) :class "pii"
                :image "nachbar.jpg" :width 1945 :height 1280
                }



               })


(defn log [text]
   (.log js/console (str text))
)

(defn safe-square-side
  "returns a square size in pixel which sill be always deisplayed"
  []
  (cond
  (> (:width @app-state) (:height @app-state)) (:height @app-state)
  (< (:width @app-state) (:height @app-state)) (:width @app-state)
  )
  )
(defn landscape []
  (> (:width @app-state) (:height @app-state))
  )

(def image-height (atom 1440))
(def background-style (atom {:width (:width @app-state)}))







(defn update-background-size
  "assure that background-image is always filling whole screen"
  [](do
    (if (landscape)
      (do
        (if (< (:width @app-state) 1280) (do (reset! image-height 720) (log "reset to 720px") ) (do (reset! image-height 720) (log "reset to 1440px") ))
      )
      (do
        (if (< (:height @app-state) 1280) (do (reset! image-height 720) (log "reset to 720px")))
      )
    )
           (swap! background-style {:width (:width @app-state)})
           (log (str  (if (landscape) "landscape:" "portrait:") " width is " (:width @app-state) " height is " (:height @app-state) ", taking image height " @image-height))
           )
)


(defn ^:export windowresize []
  (let [w (.-innerWidth js/window)
      h (.-innerHeight js/window)]
   (do
    ;(js/resetZoom)
    (swap! app-state assoc :width w)
    (swap! app-state assoc :height h)
    (update-background-size)
    )))



(defonce golden-ratio (atom (/ (+ 1 (.sqrt js/Math 5)) 2)))
(defn golden-width [](- (:width @app-state) (/ (:width @app-state) @golden-ratio)) )
(defn golden-height [] (- (:height @app-state) (/ (:height @app-state) @golden-ratio)) )

(defn golden-padding [] (* 0.1 (/ (safe-square-side) @golden-ratio)))


;; -------------------------
;; Components
(def page-number (atom 0) )

(defn image[url]
  [:image {:src url}]
  )


(defn navi-home [active]
  ;[:div.navi.animated { :class (if (or (= @page-number @pages-total) (= @page-number 0)  ) "hide0" "fadeInRight" )
  [:div.navi.animated { :class (if ( = @page-number 0  ) "hide0" "fadeInRight" )
        :style{
      :right 0
      :margin (golden-padding)
    }}
  [:a.btn {:href "#/" :on-click #(reset! page-number 0)} (if @german "  Home   " "  home   ")]
  [:a.btn {:href "#/contact" :class (if (= active 2)  "active" "")} (if @german "Kontakt   " " contact   ")]
  [:a.btn {:href "#/press" :class (if (= active 3)  "active" "")} (if @german "Presse   " "press   ")]
  [:a.btn {:href "#/about" :class (if (= active 4)  "active" "")} (if @german "Referenzen   " "about   ")]
  [:a.btn {:on-click (fn [] (german-english)) :href "javascript:void(0);" } (if @german "|ENG|" "|GER|" )]
  ]
  )

(defn navi [active]
  [:div.navi { 
        :style{
      :right 0
      :margin (golden-padding)
    }}
    [:a.btn {:href "#/" :class (if (= active 1)  "active" "")} (if @german "  Home   " "  home   ")]
  [:a.btn {:href "#/contact" :class (if (= active 2)  "active" "")} (if @german "Kontakt   " " contact   ")]
  [:a.btn {:href "#/press" :class (if (= active 3)  "active" "")} (if @german "Presse   " "press   ")]
  [:a.btn {:href "#/about" :class (if (= active 4)  "active" "")} (if @german "Referenzen    " "about   ")]
  [:a.btn {:on-click (fn [] (german-english)) :href "javascript:void(0);" } (if @german "|ENG|" "|GER|" )]
  ]
  )

(defn menu-toggle-render []
  ;[:div.btn.btn-default "Toggle Menu"]
  [:a#z.bigmac {:style{:margin (golden-padding) :margin-right (/ (golden-padding) @golden-ratio) :height (golden-padding)} :href "javascript:void(0);"} [:image {:height  (golden-padding) :src "./image/bigmac.png"}]]
  )

(defn menu-toggle-did-mount [this]
  (.click (js/$ (reagent/dom-node this))
          (fn [e]
            (.preventDefault e)
            (js/toggleMobile)
            ;(.toggleClass (js/$ "nav") "toggled") ;#wrapper will be the id of a div in our home component
            )))

(defn menu-toggle []
  ;(menu-toggle-render)
  ;)
  (reagent/create-class {:reagent-render menu-toggle-render
                         :component-did-mount menu-toggle-did-mount}))

(defn mobile-navi [active]
    [:div [:nav.animated
    [:a.btn {:style {:padding (golden-padding)} :href "#/" :class (if (= active 1) "active" "")} (if @german "Home" "home")]
    [:a.btn {:style {:padding (golden-padding)} :href "#/contact" :class (if (= active 2)  "active" "")} (if @german "Kontakt" "contact")]
    [:a.btn {:style {:padding (golden-padding)} :href "#/press" :class (if (= active 3)  "active" "")} (if @german "Presse" "press")]
    [:a.btn {:style {:padding (golden-padding)} :href "#/about" :class (if (= active 4)  "active" "")} (if @german "Referenzen" "about")]
    [:a.btn {:on-click (fn [] (german-english)) :style {:padding (golden-padding)} :href "javascript:void(0);" } (if @german "ENG" "GER" )]
      ][menu-toggle]
      ;(js/registerMobileToggle)
      ]

  )

(defn background-image[url & args]
    [:image.background.animated.infinite.zoomBG { :src (str "./image/background/" @image-height "/" url)  ;animated.infinite.zoomBG
                  :style {
                          :width (if  (landscape) (:width @app-state) (* (:height @app-state) 1.333333333) )
                          :margin-left (if (landscape) 0 (* (/ (:width @app-state) 2) -1) )
                          :margin-top (if (landscape) (* (/ (- (/ (:width @app-state) 1.333333333)  (:height @app-state) ) 2) -1)     0 )
                  }
                      }
   ]
)

(defn fighter [num]
  [:image {:src (str "/image/1_fighters/Fighter" num ".png") :style {:width (safe-square-side)}}]
)

(defn fighters[]
  ; [:div.scene {:style {:width (:width @app-state) :height (:height @app-state)}}

  [:div.page  {
              :style {  
                  :width (:width @app-state) 
                  :height (:height @app-state)
                }
          }
  [:div#fighters {:style {
                  :left (/ (:width @app-state) 2)
                  :margin-left (/ (safe-square-side) -2)
                  :width (safe-square-side)
                  :height (safe-square-side)}}
  
   ;[:div.background 
    [:image.background {:src "./image/background/1.jpg" 
                    :style {:height (safe-square-side)}}
                ];]
  (map fighter [1 2 3 4 5])
  ]
  ]
)


(defn logo-home[]
    [:div#logo.animated {:class (if (or (= @page-number @pages-total) (= @page-number 0)) "hide0" "fadeInLeft" )} 
                      [:a {:href "#/"} [:image {:src "./image/logo_ohne_weiss.png" :height (* (golden-padding) 2)
                      :style {
                        :left (golden-padding)
                        :margin (golden-padding)
                      }

                      }]]]
)


(defn logo[]
  [:div#logo.animated  [:a {:href "#/"} [:image {:src "./image/logo_ohne_weiss.png" :height (* (golden-padding) 2)
                      :style {
                        :left (golden-padding)
                        :margin (golden-padding)
                      }

                      }]]]
)

(defn logo-black[]
  [:div [:a {:href "#/"} [:image {:src "./image/logo_ohne_schwarz.png" :height (* (golden-padding) 2)
                      :style {
                        :left (golden-padding)
                        :margin (golden-padding)

                      }
                      }]]]
)

;; -------------------------
;; slider





(defonce timer (atom 0))
(def idle (atom 0))
(def seconds-left (atom 5))
(defn set-idle[]
  (log (str  "idle " @idle))
  (reset! idle 1)
  (log (str  "idle " @idle))
)

(defn reset-idle[]
  
  (reset! idle 0)
  (log (str "reset idle " @idle))
)
(defn reset-count-down[]
  
  (reset! seconds-left 9)
  (log (str "seconds-left  " @seconds-left ))
)

;(defonce pages-total (atom (- (count @sliderpages) 1)) )




; (defn swipe-element []
;   (let [swipe-state (atom {:width 0, :start-x 0, :current-x 0})]
;     (fn []
;       [:div {:on-touch-start 
;              (fn [e]
;                (reset! swipe-state {:width (.. e -target -offsetWidth)
;                                     :start-x (.. e -target -pageX)
;                                     :current-x (.. e -target -pageX)}))
;              :on-touch-move
;              (fn [e] 
;                (swap! swipe-state assoc :current-x (.. e -target -pageX)))}
;         (let [{:keys [width start-x current-x]} @swipe-state
;               direction (if (> current-x start-x) "right" "left")]
;           (str "Swipe " direction ": "
;                (* (/ (.abs js/Math (- start-x current-x)) width) 100) "%")))




(defn slp-box-up-left []
 #{
                                  ; :z-index 99
                                  :position "absolute"
                                  :margin-left (golden-padding)
                                  ; :padding-left (golden-padding)
                                  :padding-bottom (/ (golden-padding) @golden-ratio)
                                  ; :background-color "green"
                                  :top (/ (golden-height) @golden-ratio) ;(- (:height @app-state) (/ (:height @app-state) @golden-ratio))
                                  :width (golden-width);(- (:width @app-state) (/ (:width @app-state) @golden-ratio))
                                }
  )

(defn title-p [p]
  [:p p]
  )

(defn title [&body]
;todo : 

  )

(defn logo-center[]
   [:div.page  {
              :style {  
                  :width (:width @app-state) 
                  :height (:height @app-state)

                }
          }

          [:div.mittig {:style {:width  (:width @app-state)}} ;(/(* (:width @app-state) 0.55))
          [:div ;{:style{:margin-left (if (landscape) (* (:width @app-state) 0.225))}} ;(- (/ (:width @app-state) 2) (/ (* (:width @app-state) 0.55) 2))}}
                   [:image.animated.infinite.zoomBG {:src (if (landscape) "image/logo-new-black.png" "image/logo_2.png")  ;animated.infinite.zoomBG
                            :style {
                                    :width (* (:width @app-state) 0.55)
                                    ;:margin-left (- (/ (:width @app-state) 2) (* (:width @app-state) 0.275))
                                    ;:postion "absolute"
                                    ;:top (* (:height @app-state) 0.5)
          
                                    ;:width (if (landscape) (:width @app-state) (* (:height @app-state) 1.333333333) )
                                    ;:margin-left (if (landscape)  (- (/ (:width @app-state) 2) (* (:width @app-state) 0.275)) 0 )
                                    ;:margin-top (if (landscape) (* (/ (- (:width @app-state)   (:height @app-state) ) 2) -1)     0 )
                              }
                            }
                   ]
          ]
   ]

  ]
  )


;todo make page
(defn page [id classs &body]
  [:div.page  {
            :id id
              :style {  
                  :width (:width @app-state) 
                  :height (:height @app-state)
                }
            :class classs
          }

          ;todo deconstruct body
  ]
  )

(defn slider-page[sliderpage]
  ; [:div.page {:style {:width (:width @app-state) :height (* 10 (/ (:width @app-state) 16))}}
  [:div.page  {
            :id (:key sliderpage)
              :style {  
                  :width (:width @app-state) 
                  :height (:height @app-state)
                }
            :class (:class sliderpage)
          }
      [background-image (:image sliderpage)]
      [:div.gradient {:style{
                    :top 0
                    :opacity 0
                    :width (+  (golden-padding) (golden-width))
                    :position "absolute"
                    :height (:height @app-state)
              }}
      ]

      [:div.slp-box.bar.gradient { ;:class (if (> @page-number 1) "gradient" "")
                                
                                :style{
                                  ; :z-index 99
                                  :position "absolute"
                                  :padding (golden-padding)
                                  :padding-top (golden-padding)
                                  :padding-bottom (golden-padding)
                                  :max-width (if (landscape) (/ (safe-square-side) @golden-ratio) (- (safe-square-side)  (golden-padding)) )
                                  
          
                                }
        } [:h1.slp-title {:style
             {
            :font-size (* (golden-padding) 1.15)
            :line-height (str (* (golden-padding) 1.05) "px")
             }
           }
          (if @german 
                (if (seq? (:title-de sliderpage)) (map title-p (:title-de sliderpage)) (:title-de sliderpage))
                (if (seq? (:title-en sliderpage)) (map title-p (:title-en sliderpage)) (:title-en sliderpage))
            )

          ]  ;]
        [:p.slp-text 

        (if @german
                (if (seq? (:text-de sliderpage)) (map title-p (:text-de sliderpage)) (:text-de sliderpage))
                (if (seq? (:text-en sliderpage)) (map title-p (:text-en sliderpage)) (:text-en sliderpage))

          ) 
        ]
      ]

  ]
 )

(defn slider-pages 
  "slider pages component"
  []
  (let [swipe-state (atom {:width 0, :start-x 0, :current-x 0})]
    ; (doall
      
  [:div.slider-pages {:style {:width (* (:width @app-state) (+ 1 @pages-total) ) :height (:height @app-state)
                ; :transform [:translate3d "(-1000px,0,0)"] 
                ; todo: touch event
               :transform (str "translate3d(-" (* @page-number (:width @app-state)) "px,0,0)")
                }
              :on-click (fn [e] (js/toggleBarText))
               :on-touch-start 
                    (fn [e]
                      ;todo swipe
                      ;(log "touch start")
                     ;(js/toggleBarText) 
                     (reset! swipe-state {:width (.. e -target -offsetWidth)
                                          :start-x (.. e -target -pageX)
                                          :current-x (.. e -target -pageX)}))
                   :on-touch-move
                   (fn [e] 
                     (swap! swipe-state assoc :current-x (.. e -target -pageX))
                      ;(log (:current-x @swipe-state))
                      ;
                     )

                  ;  (let [{:keys [width start-x current-x]} @swipe-state
                    ;     direction (if (> current-x start-x) "right" "left")]
                    ; (str "Swipe " direction ": "
                    ;      (* (/ (.abs js/Math (- start-x current-x)) width) 100) "%"))
               }
  ; PUT IN HERE THE PAGES !

          ; (map slider-page (:sliderpages @app-state))


          ; (slider-page (nth sliderpages 0))
          ; (map slider-page sliderpages)
        ; (doall 
          ; (for [page (:sliderpages @app-state)]

          ; ; (for [page sliderpages]
          ; ;    ^{:key page} 
          ; ;   (log  page)
          ;     ; (slider-page page)
        ;         (if (= page (nth (:sliderpages @app-state) 0 )) (fighters) (slider-page page) )
        ;       )
        ;     )

        ;(fighters)
        (logo-center)
        (slider-page (:slide1 sliderpages))
        (slider-page (:slide2 sliderpages))
        (slider-page (:slide3 sliderpages))
        (slider-page (:slide4 sliderpages))
        (slider-page (:slide5 sliderpages))
        (slider-page (:slide6 sliderpages))
        (slider-page (:slide7 sliderpages))
        (slider-page (:slide8 sliderpages))
        (logo-center)
  
  ] 
    )
  )

(defn slide-left[]
    (if (> @page-number 0) (swap! page-number dec) (log "not swapped page-number"))
    
    
)
(defn slide-right[]
  (if  (< @page-number @pages-total) (swap! page-number inc) (reset! page-number 0))
  )


(defn prev-btn-handler[]
  (do
    (js/showBarText)
   (reset-count-down)
   (slide-left)
   (windowresize)
   )
  )
(defn prev-btn []
  [:div#slider-prev.slider-btn {:on-click prev-btn-handler
                          :style {
                        ;:left (golden-padding)
                        :margin-left (golden-padding)
                        ;:margin-bottom (* (golden-padding) 0.5)
                      }
                      } [:img {:src "image/pfeil.png", :alt "<", :height (* (golden-padding) 1.2)}] ]
  )



(defn next-btn-handler[]
  (do
    (js/showBarText)
    (log "next-btn pressd")
    (log @page-number)
   (reset-count-down)
   (slide-right)
   (windowresize)
   )
  )
(defn next-btn []
  [:div#slider-next.slider-btn  {:on-click next-btn-handler 
                      :style {
                        :margin-right (golden-padding)
                      }} [:img {:src "image/pfeil_2.png", :alt ">", :height (* (golden-padding) 1.2)}] 
  ]
)


(defn count-down-handler []
  (do
   (log @seconds-left)
   (if (> @seconds-left 0) (swap! seconds-left dec)
   )
   
  )
)

(defn check-idle[]
  (do ;(log "tick")
   (if (< @seconds-left 1) (slide-right)))
  )

;(defonce idle-checker (js/setInterval
;                ; #(reset! timer (js/Date.)) 1000))
;                check-idle 20000)
;)

;(defonce count-down  (js/setInterval
;                count-down-handler 1000)
;)




(defn slider[]

  [:div#slider {:style {:width (:width @app-state) :height (:height @app-state)
                ; :transform "translate3d(0,0,0)"
               }}
    ; [:div.gradient {:style{
    ;                 :top 0
    ;                 :z-index 59
    ;                 :width (+  (golden-padding) (golden-width))
    ;                 :position "absolute"
    ;                 :height (:height @app-state)
    ;           }}
    ;   ]
    
       [slider-pages]
    (if (> @page-number 0           ) [prev-btn])
    (if (< @page-number @pages-total) [next-btn])

    ]

  ;(@page-number)
  ; [fighters]  
)




;; -------------------------
;; Views

;(defn current-page []
;  [:div [(session/get :current-page)]])


(defn home-page []
  [:div
  (windowresize)
  (js/disableScroll)
  ;(reset! page-number 0)
  ;(log (:page @page-state))
  [logo-home]
  [navi-home 1]
   [slider]
   [mobile-navi 1]
  ]



  )

(defn contact-page[]
  [:div
  (js/disableScroll)
  [logo]
  [navi 2]
  ;[background-image "kontakt.jpg" ]
  [:image.background.animated.rollIn { :src (str "./image/background/" @image-height "/" "kontakt.jpg")  ;animated.infinite.zoomBG
                  :style {
                          :width (if (landscape) (:width @app-state) (* (:height @app-state) 1.333333333) )
                          :margin-left (if (landscape) 0 (* (/ (:width @app-state) 2) -1) )
                          :margin-top (if (landscape) (* (/ (- (/ (:width @app-state) 1.333333333)  (:height @app-state) ) 6) -1)     0 )
                  }
                      }
   ]





  [:div.slp-box.bar.gradient { 
                                
                                :style{
                                  ; :z-index 99
                                  :position "absolute"
                                  :margin (golden-padding)
                                  :margin-left 0
                                  :padding (golden-padding)
                                  ;:padding-bottom (golden-padding)
                                  :top (if (landscape) (/(:height @app-state) 4.5) (/(:height @app-state) 2.4))

                                  ; :padding-left (golden-padding)
                                  ;:padding-bottom (/ (golden-padding) @golden-ratio)
                                  ; :background-color "green"
                                  ;:bottom (golden-padding)  ;(- (:height @app-state) (/ (:height @app-state) @golden-ratio))
                                  :max-width 550;(- (:width @app-state) (/ (:width @app-state) @golden-ratio))
                                }
        } [:h1.slp-title {:style
             {
            :font-size (* (golden-padding) 1.15)
            :line-height (str (* (golden-padding) 1.05) "px")
             }} 
              ;^-(if @german "Kontakt" "contact")
              ]
         
              ;]
        [:p.slp-text "THE KEY GmbH"]
        [:p "Sedanstrasse 6"]
        [:p "D-83022 Rosenheim"]
        [:p "Managing Director:"]
        [:p "Sandra Gross"]
        [:p  [:a {:style {:color "white" :font-size "1.5em"} :href "mailto:info@thekey.cc"} (if @german "E-Mail" "email")]]
        ]
  [mobile-navi 2]
  ]
  )

(defn press-page[]
  [:div
  [logo]
  (js/disableScroll)
  [navi 3]
  ;[background-image "presse-schwarz.jpg"]
  [:image.background.animated.infinite.zoomBG { :src (str "./image/background/" @image-height "/" "presse.jpg")  ;animated.infinite.zoomBG
                  :style {
                          :width (if (landscape) (:width @app-state) (* (:height @app-state) 1.333333333) )
                          :margin-left (if (landscape) 0 (* (/ (:width @app-state) 1) -1) )
                          ;:margin-top (if (landscape) (* (/ (- (:width @app-state)  (:height @app-state) ) 6) -1)     0 )
                  }
                      }
   ]
  [:div.text_unten
    [:div.fixed.gradient
     ;{:style ""}
     {:style {
                            :max-width (if (landscape) (/ (safe-square-side) @golden-ratio) (- (safe-square-side)  (golden-padding)) )
                             :padding (golden-padding)
                             ;:padding-left (golden-padding)
                            ; :margin (golden-padding)
                           }}
     [:h1 {:style
             {
            :font-size (* (golden-padding) 1.15)
            :line-height (str (* (golden-padding) 1.05) "px")
             }} (if @german "INTERNATIONALE PRESSE" "INTERNATIONAL PRESS")]
     [:p.slp-text
      (if @german  "Lesen Sie hier Artikel der internationalenPresse über unseren Service." 
                    "Here, you can find articles from the international press about our services.")
      ]
     [:a.download {:href "./Presse-THE-KEY.pdf"} "DOWNLOAD"]]]
  [mobile-navi 3]
  ]
  )


(defn about-page []
  [:div {:style { 
                  :background-color "black"
                  :width (:width @app-state) 
                  :height (:height @app-state)
                  :overflow "scroll"
                }}
  [:div {:style{:background-color "black" :width "100%" :height (*(golden-padding) 3)
          :position "fixed"

        }} ]
  [logo]

  (js/enableScroll)
  [navi 4]

   [:div.main {:style {
       :margin (golden-padding)
       :margin-top (*(:height @app-state) 0.16)

       }}
    [:div.jahre
     [:h1.big (if @german "25 JAHRE" "25 years")]
     [:h2.bigger
      (if @german "Internationale Erfahrung für global player im Luxussegment" 
                "of international experience for global players in the luxury segment")]]
    [:div.aufzaehl
     [:ul.abstand

      [:li (if @german "PORSCHE weltweiter Reveal/Präsentation des Porsche 911 (997)" 
                    "PORSCHE worldwide reveal/presentation of the Porsche 911 (997)")]
      [:li (if @german "BMW International Open" 
                    "BMW International Open")]
      [:li (if @german "BMW Clean Energy Worldtour" 
                    "BMW CleanEnergy World Tour ")]
      [:li (if @german "BMW Skiweltcup Kitzbühel" 
                    "BMW Skiing World Cup Kitzbühel")]
      [:li (if @german "BMW 12-Zylinder Golf Cup unter der Schirmherrschaft von Dr. Wolfgang Reitzle mit Teilnehmern aus der Europäischen Wirtschaft" 
                    "BMW 12-cylinder Golf Cup under the patronage of Dr. Wolfgang Reitzle with participants from the European business world ")]
      [:li (if @german "BMW Händlerpräsentation 7er Langversion (E65) in Asien für EMEA BMW AG Vorstandsverabschiedungen" 
                    "BMW dealer presentation of the 7 Series long wheelbase version (E65) in Asia for EMEA BMW AG Management Board leaving celebrations")]
      [:li (if @german "MARC O'POLO Image Kampagne & Reveal für die skandinavischen Märkte in Amsterdam" 
                    "MARC O'POLO image campaign and reveal for the Scandinavian markets in Amsterdam")]
      [:li (if @german "BREITLING ID CLUB Baselworld seit 2001 " 
                    "BREITLING ID CLUB at Baselworld since 2001")]
      [:li (if @german "BREILTING Boutiquen weltweit; Outfit-Design und Produktion für alle Sales Agents" 
                        "BREITLING boutiques worldwide; outfit design and production for all sales agents   ")]
      [:li (if @german "Flugzeugdesign des BREITLING Jet Teams" 
                      "Aircraft design for the BREITLING Jet Team")]
      [:li (if @german "Design und Produktion der Piloten Outfits des BREITLING Jet Teams " 
                    "Design and production of the pilots' outfits for the BREITLING Jet Team ")]
      [:li (if @german "Flugzeugdesign des Breitling Racing Teams für das Red Bull Airrace" 
                    "Aircraft design for the Breitling Racing Team for the Red Bull Air Race ")]
      [:li (if @german "Design und Produktion der Piloten Outfits für das Breitling Racing Team" 
                    "Design and production of the pilots' outfits for the Breitling Racing Team")]
      [:li (if @german "BREITLING Sponsoring Repräsentative für das Red Bull Airrace seit 2007" 
                    "BREITLING sponsorship representative for the Red Bull Air Race since 2007")]
      



       ]]]
  [mobile-navi 4]
  ])



;; -------------------------
;; Routes



(defn app-routes []

  ( secretary/set-config! :prefix "#" )

  (defroute "/" []
    (swap! page-state assoc :page :home))

  (defroute "/contact" []
    (swap! page-state assoc :page :contact))

  (defroute "/press" []
    (swap! page-state assoc :page :press))

  (defroute "/about" []
    (swap! page-state assoc :page :about))

  (hook-browser-navigation!)

)


(defmulti current-page #(@page-state :page))
(defmethod current-page :home [] 
  [home-page])
(defmethod current-page :contact [] 
  [contact-page])
(defmethod current-page :press [] 
  [press-page])
(defmethod current-page :about [] 
  [about-page])
(defmethod current-page :default [] 
  [:div ])
;; -------------------------
;; Initialize app




(defn windowresize-handler
  [event]
  (windowresize))

(.addEventListener js/window "resize" windowresize-handler)
 ;(.addEventListener touchstart)


 (defn mount-root []
  (do 
    (.initializeTouchEvents js/React true)
    (log "Rmount")
  (reagent/render [current-page] (.getElementById js/document "app"))
  ;(js/registerMobileToggle)
  )
)
 (defn init! []
  (do
    (log "init!")
    (app-routes)
    ;(accountant/configure-navigation!)
    ;(accountant/dispatch-current!)
    (mount-root)))





(defn ^:export main []
  (do
    (app-routes)

  (.initializeTouchEvents js/React true)
  (reagent/render [current-page]
                  (.getElementById js/document "app"))))
