(ns thekey-www.prod
  (:require [thekey-www.core :as core]))

;;ignore println statements in prod
(set! *print-fn* (fn [& _]))

(core/init!)
