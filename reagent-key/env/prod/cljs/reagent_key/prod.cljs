(ns reagent-key.prod
  (:require [reagent-key.core :as core]))

;;ignore println statements in prod
(set! *print-fn* (fn [& _]))

(core/init!)
