// Compiled by ClojureScript 1.7.170 {}
goog.provide('cljs.repl');
goog.require('cljs.core');
cljs.repl.print_doc = (function cljs$repl$print_doc(m){
cljs.core.println.call(null,"-------------------------");

cljs.core.println.call(null,[cljs.core.str((function (){var temp__4425__auto__ = new cljs.core.Keyword(null,"ns","ns",441598760).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_(temp__4425__auto__)){
var ns = temp__4425__auto__;
return [cljs.core.str(ns),cljs.core.str("/")].join('');
} else {
return null;
}
})()),cljs.core.str(new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1(m))].join(''));

if(cljs.core.truth_(new cljs.core.Keyword(null,"protocol","protocol",652470118).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.call(null,"Protocol");
} else {
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"forms","forms",2045992350).cljs$core$IFn$_invoke$arity$1(m))){
var seq__10440_10454 = cljs.core.seq.call(null,new cljs.core.Keyword(null,"forms","forms",2045992350).cljs$core$IFn$_invoke$arity$1(m));
var chunk__10441_10455 = null;
var count__10442_10456 = (0);
var i__10443_10457 = (0);
while(true){
if((i__10443_10457 < count__10442_10456)){
var f_10458 = cljs.core._nth.call(null,chunk__10441_10455,i__10443_10457);
cljs.core.println.call(null,"  ",f_10458);

var G__10459 = seq__10440_10454;
var G__10460 = chunk__10441_10455;
var G__10461 = count__10442_10456;
var G__10462 = (i__10443_10457 + (1));
seq__10440_10454 = G__10459;
chunk__10441_10455 = G__10460;
count__10442_10456 = G__10461;
i__10443_10457 = G__10462;
continue;
} else {
var temp__4425__auto___10463 = cljs.core.seq.call(null,seq__10440_10454);
if(temp__4425__auto___10463){
var seq__10440_10464__$1 = temp__4425__auto___10463;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__10440_10464__$1)){
var c__6133__auto___10465 = cljs.core.chunk_first.call(null,seq__10440_10464__$1);
var G__10466 = cljs.core.chunk_rest.call(null,seq__10440_10464__$1);
var G__10467 = c__6133__auto___10465;
var G__10468 = cljs.core.count.call(null,c__6133__auto___10465);
var G__10469 = (0);
seq__10440_10454 = G__10466;
chunk__10441_10455 = G__10467;
count__10442_10456 = G__10468;
i__10443_10457 = G__10469;
continue;
} else {
var f_10470 = cljs.core.first.call(null,seq__10440_10464__$1);
cljs.core.println.call(null,"  ",f_10470);

var G__10471 = cljs.core.next.call(null,seq__10440_10464__$1);
var G__10472 = null;
var G__10473 = (0);
var G__10474 = (0);
seq__10440_10454 = G__10471;
chunk__10441_10455 = G__10472;
count__10442_10456 = G__10473;
i__10443_10457 = G__10474;
continue;
}
} else {
}
}
break;
}
} else {
if(cljs.core.truth_(new cljs.core.Keyword(null,"arglists","arglists",1661989754).cljs$core$IFn$_invoke$arity$1(m))){
var arglists_10475 = new cljs.core.Keyword(null,"arglists","arglists",1661989754).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_((function (){var or__5330__auto__ = new cljs.core.Keyword(null,"macro","macro",-867863404).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_(or__5330__auto__)){
return or__5330__auto__;
} else {
return new cljs.core.Keyword(null,"repl-special-function","repl-special-function",1262603725).cljs$core$IFn$_invoke$arity$1(m);
}
})())){
cljs.core.prn.call(null,arglists_10475);
} else {
cljs.core.prn.call(null,((cljs.core._EQ_.call(null,new cljs.core.Symbol(null,"quote","quote",1377916282,null),cljs.core.first.call(null,arglists_10475)))?cljs.core.second.call(null,arglists_10475):arglists_10475));
}
} else {
}
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"special-form","special-form",-1326536374).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.call(null,"Special Form");

cljs.core.println.call(null," ",new cljs.core.Keyword(null,"doc","doc",1913296891).cljs$core$IFn$_invoke$arity$1(m));

if(cljs.core.contains_QMARK_.call(null,m,new cljs.core.Keyword(null,"url","url",276297046))){
if(cljs.core.truth_(new cljs.core.Keyword(null,"url","url",276297046).cljs$core$IFn$_invoke$arity$1(m))){
return cljs.core.println.call(null,[cljs.core.str("\n  Please see http://clojure.org/"),cljs.core.str(new cljs.core.Keyword(null,"url","url",276297046).cljs$core$IFn$_invoke$arity$1(m))].join(''));
} else {
return null;
}
} else {
return cljs.core.println.call(null,[cljs.core.str("\n  Please see http://clojure.org/special_forms#"),cljs.core.str(new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1(m))].join(''));
}
} else {
if(cljs.core.truth_(new cljs.core.Keyword(null,"macro","macro",-867863404).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.call(null,"Macro");
} else {
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"repl-special-function","repl-special-function",1262603725).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.call(null,"REPL Special Function");
} else {
}

cljs.core.println.call(null," ",new cljs.core.Keyword(null,"doc","doc",1913296891).cljs$core$IFn$_invoke$arity$1(m));

if(cljs.core.truth_(new cljs.core.Keyword(null,"protocol","protocol",652470118).cljs$core$IFn$_invoke$arity$1(m))){
var seq__10444 = cljs.core.seq.call(null,new cljs.core.Keyword(null,"methods","methods",453930866).cljs$core$IFn$_invoke$arity$1(m));
var chunk__10445 = null;
var count__10446 = (0);
var i__10447 = (0);
while(true){
if((i__10447 < count__10446)){
var vec__10448 = cljs.core._nth.call(null,chunk__10445,i__10447);
var name = cljs.core.nth.call(null,vec__10448,(0),null);
var map__10449 = cljs.core.nth.call(null,vec__10448,(1),null);
var map__10449__$1 = ((((!((map__10449 == null)))?((((map__10449.cljs$lang$protocol_mask$partition0$ & (64))) || (map__10449.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__10449):map__10449);
var doc = cljs.core.get.call(null,map__10449__$1,new cljs.core.Keyword(null,"doc","doc",1913296891));
var arglists = cljs.core.get.call(null,map__10449__$1,new cljs.core.Keyword(null,"arglists","arglists",1661989754));
cljs.core.println.call(null);

cljs.core.println.call(null," ",name);

cljs.core.println.call(null," ",arglists);

if(cljs.core.truth_(doc)){
cljs.core.println.call(null," ",doc);
} else {
}

var G__10476 = seq__10444;
var G__10477 = chunk__10445;
var G__10478 = count__10446;
var G__10479 = (i__10447 + (1));
seq__10444 = G__10476;
chunk__10445 = G__10477;
count__10446 = G__10478;
i__10447 = G__10479;
continue;
} else {
var temp__4425__auto__ = cljs.core.seq.call(null,seq__10444);
if(temp__4425__auto__){
var seq__10444__$1 = temp__4425__auto__;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__10444__$1)){
var c__6133__auto__ = cljs.core.chunk_first.call(null,seq__10444__$1);
var G__10480 = cljs.core.chunk_rest.call(null,seq__10444__$1);
var G__10481 = c__6133__auto__;
var G__10482 = cljs.core.count.call(null,c__6133__auto__);
var G__10483 = (0);
seq__10444 = G__10480;
chunk__10445 = G__10481;
count__10446 = G__10482;
i__10447 = G__10483;
continue;
} else {
var vec__10451 = cljs.core.first.call(null,seq__10444__$1);
var name = cljs.core.nth.call(null,vec__10451,(0),null);
var map__10452 = cljs.core.nth.call(null,vec__10451,(1),null);
var map__10452__$1 = ((((!((map__10452 == null)))?((((map__10452.cljs$lang$protocol_mask$partition0$ & (64))) || (map__10452.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__10452):map__10452);
var doc = cljs.core.get.call(null,map__10452__$1,new cljs.core.Keyword(null,"doc","doc",1913296891));
var arglists = cljs.core.get.call(null,map__10452__$1,new cljs.core.Keyword(null,"arglists","arglists",1661989754));
cljs.core.println.call(null);

cljs.core.println.call(null," ",name);

cljs.core.println.call(null," ",arglists);

if(cljs.core.truth_(doc)){
cljs.core.println.call(null," ",doc);
} else {
}

var G__10484 = cljs.core.next.call(null,seq__10444__$1);
var G__10485 = null;
var G__10486 = (0);
var G__10487 = (0);
seq__10444 = G__10484;
chunk__10445 = G__10485;
count__10446 = G__10486;
i__10447 = G__10487;
continue;
}
} else {
return null;
}
}
break;
}
} else {
return null;
}
}
});
