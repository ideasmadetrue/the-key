(defproject reagent-key "0.1.0-SNAPSHOT"
  :dependencies [[org.clojure/clojure "1.7.0"]
                 [org.clojure/clojurescript "1.7.122"]
                 [reagent "0.5.1"
                  :exclusions [org.clojure/tools.reader]]
                 [reagent-utils "0.1.5"]
                [hiccup "1.0.5"]
                 [secretary "1.2.3"]]

  :source-paths ["src/clj"]
  :min-lein-version "2.5.0"

  :plugins [[lein-cljsbuild "1.1.1"]
  			[lein-asset-minifier "0.2.2"
             :exclusions [org.clojure/clojure]]]

  :clean-targets ^{:protect false} ["resources/public/js/compiled" "target" "test/js"]
  :minify-assets
    {:assets
                  {"resources/public/css/site.min.css" "resources/public/css/site.css"}}

  :cljsbuild {:builds [{:id "prod"
                        :source-paths ["src/cljs"]
                        :compiler {:output-to "resources/public/js/compiled/app.js"
                                   :optimizations :advanced
                                   :pretty-print false}}]})
